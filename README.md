This package has been absorbed by [`gstreamer`][gst].

[gst]: https://gitlab.archlinux.org/archlinux/packaging/packages/gstreamer
